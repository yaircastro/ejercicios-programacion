#include <stdio.h>

int main ()
{
	int numero = 0;
	printf("Este es un programa para realizar una suma de la serie que va desde 1 hasta el valor ingresado por el usuario.\n");
	printf("Ingrese un numero entre el 1 y el 50\n");
	scanf("%i", &numero);
	int suma = numero;
	numero--;
	if (numero >= 1 & numero <= 50) 
	{
		for (numero; numero >= 1; numero--)
		{
			suma = suma + numero;
		}
		printf("La suma del 1 hasta el valor ingresado es igual a: %i", suma);
	}
	else 
	{
		printf("Su numero no esta dentro del rango de 1 y 50. Intente nuevamente.");
	}
	return 0;
}
