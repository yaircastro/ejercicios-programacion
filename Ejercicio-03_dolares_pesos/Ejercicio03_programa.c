#include <stdio.h>

float dolares_pesos (float dolares, float pesos);

int main ()
{
	float dolares, pesos, resultado;
	printf("Este es un programa para calcular la equivalencia de dolares a pesos.\n");
	printf("Ingrese una cantidad en dolares:\n");
	scanf("%f", &dolares);
	pesos = dolares_pesos(dolares, pesos);
	printf("%.2f USD es igual a %.2f pesos mxn.", dolares, pesos);
	return 0;
}

float dolares_pesos(float dolares, float pesos)
{
	float equiv = 20.02;
	pesos = dolares*equiv;
	return pesos;
}
