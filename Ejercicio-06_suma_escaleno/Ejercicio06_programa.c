#include <stdio.h>

float calc_perim(float a, float b, float c);
void mensaje();

int main ()
{
	float a, b, c, perimetro;
	char x;

	printf("Este es un programa para calcular el perimetro de un triangulo escaleno.\n");
	mensaje(x = 'a');
	scanf("%f", &a);
	mensaje(x = 'b');
	scanf("%f", &b);
	mensaje(x = 'c');
	scanf("%f", &c);
	perimetro = calc_perim(a, b, c);
	printf("El perimetro del triangulo segun los datos proporcionados es igual a %.2f", perimetro);
}


float calc_perim(float a, float b, float c)
{
	float perimetro;
	perimetro = a + b + c;
	return perimetro;
}

void mensaje(x) 
{
	printf("Ingrese la longitud del lado \"%c\" del triangulo: \n", x);
}
