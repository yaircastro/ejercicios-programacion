#include <stdio.h>

float perimetro_equilatero (float lado);

int main ()
{
	float lado, perimetro;
	printf("Este es un programa para calcular el perimetro de un triangulo equilatero.\n");
	perimetro = perimetro_equilatero(lado);
	printf("El perimetro del triangulo es: %.2f", perimetro);
	return 0;
}

float perimetro_equilatero(float lado) 
{
	float perimetro;
	printf("Ingrese la longitud de uno de los lados del triangulo: \n");
	scanf("%f", &lado);
	perimetro = lado * 3;
	return perimetro;
}
