#include <stdio.h>

float perimetro_isosceles(float base, float lado);

int main ()
{
	float base, lado, perimetro;
	printf("Este es un programa para calcular el perimetro de un triangulo isosceles.\n");
	perimetro = perimetro_isosceles(base, lado);
	printf("El perimetro del triangulo isosceles es: %.2f", perimetro);
	return 0;
}

float perimetro_isosceles(float base, float lado)
{
	float perimetro;
	printf("Ingrese la longitud de la base del triangulo: \n");
	scanf("%f", &base);
	printf("Ingrese la longitud de uno de los lados del triangulo: \n");
	scanf("%f", &lado);
	perimetro = base + lado * 2;
	return perimetro;
}
